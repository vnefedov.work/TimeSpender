import json
import os
import random

import boto3
import vk

aws_access_key_id = os.environ.get('AWS_ACCESS_KEY')
aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
aws_s3_bucket_name = os.environ.get('AWS_S3_BUCKET_NAME')

vk_service_token = os.environ.get('VK_SERVICE_TOKEN')


def get_current_vk_id():
    environ_vk_id = os.environ.get('VK_ID')
    if not environ_vk_id:
        environ_vk_id = random.randint(10000, 99999)

    return environ_vk_id


def get_friends(current_vk_id):
    vk_session = vk.Session(access_token=vk_service_token)
    vk_api = vk.API(vk_session)
    return vk_api.friends.get(
        user_id=current_vk_id,
        fields='firstname,lastname',
        v='5.80')


def serialize_friend_list(friend_list):
    return json.dumps(friend_list)


def save_friends_to_s3(friend_list, current_vk_id):
    serialized_list = serialize_friend_list(friend_list)
    file_contents = bytes(serialized_list, 'utf-8')

    s3 = boto3.client(
        's3',
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key)

    return s3.put_object(
        ACL='public-read',
        Body=file_contents,
        Bucket=aws_s3_bucket_name,
        Key='friends/friends_of_{}.txt'.format(current_vk_id))


def main():
    current_vk_id = get_current_vk_id()
    friend_list = get_friends(current_vk_id)
    save_friends_to_s3(friend_list, current_vk_id)


if __name__ == '__main__':
    main()
